/*
 * This Spock specification was generated by the Gradle 'init' task.
 */
package testing.spock

import spock.lang.Specification

class AppTest extends Specification {
    def "application has a greeting"() {
        setup:
        def app = new App()

        when:
        def result = app.greeting

        then:
        result != null
    }

    def "test Example Method"() {
        given: "new class App"
        def app = new App()
        when: "invoke greeting"
        def result = app.greeting
        then: "say Hello"
        "Hello" == result
    }

}
